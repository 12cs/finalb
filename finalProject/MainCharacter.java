import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * This is the waiter or waitress. They pick up the food, serve it to customers and dump unwanted food. 
 * 
 * Jamie, Joan, Gloria, Sandy
 * @January 16, 2019
 */
public class MainCharacter extends Actor
{
    private Food food;
    //
    public int character = -1;
    //this makes sure the main character does not hit this upper bound (the table is actualy the background so I don't want it 
    //to walk all over the background)
    private int minY = 250;
    //helps to check if food should be regenerated
    private int[] foodPickUps = new int[]{ 0,0,0,0,0,0,0,0 };
    private GameScreen gs;
    //checks to see if the main character is near a customer table
    private static boolean tableNear = false;
    private static boolean customerEating = false;
    public MainCharacter(GameScreen gs)
    {
        this.gs = gs;

    }

    public void addedToWorld(String img)
    {
        setImage(img);
    }

    public void act() 
    {
        //movement keys that allow the character to move in all directions 
        if(Greenfoot.isKeyDown("a"))
        {
            setLocation(getX()-4, getY());
        }
        if(Greenfoot.isKeyDown("d"))
        {
            setLocation(getX()+4, getY());
        }
        if(Greenfoot.isKeyDown("w"))
        {
            setLocation(getX(), getY()-4);
        }
        if(Greenfoot.isKeyDown("s"))
        {
            setLocation(getX(), getY()+4);
        }
        //"q" is to pick up the food if it is in range
        if(food == null && Greenfoot.isKeyDown("q") && !getObjectsInRange(90, Food.class).isEmpty())
        {
            //make sure the nearest food is the food being picked up
            try {
                List<Food> nearFoods = getObjectsInRange(90, Food.class);
                double nearestDistance = 100.0;
                double distance = 100.0;
                for (int i = 0; i < nearFoods.size(); i++) {
                    distance = getDistance(nearFoods.get(i));
                    if (distance < nearestDistance) {
                        food = nearFoods.get(i);
                        nearestDistance = distance;
                    }
                }

                if (food.getFoodEaten()) {
                    food = null;
                }
            }
            catch(Exception e) {
            }              
        }
        if(food != null)
        {
            checkDrop();
        }
        //allows the character to not walk over the "food" table
        while (getY() < minY) 
        {
            setLocation(getX(), getY()+5);
        }
        //checks if in range of the customer tables 
        if(!getObjectsInRange(150, Table.class).isEmpty()){
            tableNear = true;
        }
        else{
            tableNear = false;
        }
    }

    public void setLocation (int x, int y) 
    {
        //by overiding this method the character moves the food when picked up 
        super.setLocation(x, y);
        if(food != null)
        {
            food.setLocation(x, y+5);
        }
    }
    //getter method to return the distance from food 
    private double getDistance(Food food) {
        return Math.hypot(food.getX() - getX(), food.getY() - getY());
    }
    //getter method to return if the character is near the customer table
    public static boolean nearTable() {
        return tableNear;
    }

    public void checkDrop()
    {
        //key is the most recent key pressed
        String key = Greenfoot.getKey();
        if(key != null)
        {
            //"e" is the control used to drop the food 
            if(key.equals("e") && this.getY() > minY)
            {
                //when food is dropped and within these coordinates, food snaps into place on table
                if(this.getY() > 335 && this.getY() < 535)
                {
                    if(this.getX() < 700)
                    {
                        //pushes food to the next available spot on the table to make sure food does not stack
                        if(this.getX() < 460 && getWorld().getObjectsAt(420, 420, Food.class).isEmpty())
                        {
                            food.setLocation(420, 420);
                        }
                        else if (this.getX() < 540 && getWorld().getObjectsAt(500, 420, Food.class).isEmpty()) 
                        {
                            food.setLocation(500, 420);
                        }
                        else 
                        {
                            if(getWorld().getObjectsAt(580, 420, Food.class).isEmpty()) {
                                food.setLocation(580, 420);
                            }
                        }
                    }
                    else
                    {
                        if(this.getX() < 860 && getWorld().getObjectsAt(820, 420, Food.class).isEmpty())
                        {
                            food.setLocation(820, 420);
                        }
                        else if (this.getX() < 940 && getWorld().getObjectsAt(900, 420, Food.class).isEmpty())
                        {
                            food.setLocation(900, 420);
                        }
                        else 
                        {
                            if(getWorld().getObjectsAt(980, 420, Food.class).isEmpty()) {
                                food.setLocation(980, 420);
                            }
                        }
                    }
                    food.checkTimeToEat();
                }
                else if(this.getY() > 535 && this.getY() < 735)
                {
                    if(this.getX() < 700)
                    {
                        if(this.getX() < 460 && getWorld().getObjectsAt(420, 620, Food.class).isEmpty())

                        {
                            food.setLocation(420, 620);
                        }
                        else if (this.getX() < 540 && getWorld().getObjectsAt(500, 620, Food.class).isEmpty())
                        {
                            food.setLocation(500, 620);
                        }
                        else 
                        {
                            if(getWorld().getObjectsAt(580, 620, Food.class).isEmpty())
                            {
                                food.setLocation(580, 620);
                            }
                        }
                    }
                    else
                    {
                        if(this.getX() < 860 && getWorld().getObjectsAt(820, 620, Food.class).isEmpty())
                        {
                            food.setLocation(820, 620);
                        }
                        else if (this.getX() < 940 && getWorld().getObjectsAt(900, 620, Food.class).isEmpty())
                        {
                            food.setLocation(900, 620);
                        }
                        else 
                        {
                            if(getWorld().getObjectsAt(980, 635, Food.class).isEmpty())
                            {
                                food.setLocation(980, 620);
                            }
                        }
                    }
                    food.checkTimeToEat();
                }
                //indicates food is picked up, increment the num of times it was
                if(food.getInitialPick()) 
                {
                    food.setInitialPick();
                    foodPickUps[food.getFoodType()] += 1;
                }
                //if it has been a multiple of 3, restock the food
                if(foodPickUps[food.getFoodType()] % 3 == 0) 
                {
                    gs.restock(food.getFoodType());
                }
                food = null;
            }
        }   
    }
}
