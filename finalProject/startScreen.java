import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Initiates start screen world
 * 
 * @Joan, Gloria, Jamie, Sandy
 * @Jan 16, 2019
 */
public class StartScreen extends World
{

    /**
     * Initiates basic world
     */
    public StartScreen()
    {    
        // Create a new world with 1200x800 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1); 
        Buttons();
    }

    //Adds buttons to screen
    private void Buttons()
    {
        Button start = new Button("start button.png",new CharacterScreen(), false, this, "");
        addObject(start, 395, 680);
        Button instructions = new Button("instructions button.png", new InstructionScreen(), false, this, "");
        addObject(instructions, 795, 680);
    }
}
