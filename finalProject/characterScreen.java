import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class allows the player to choose a character
 * 
 * @Gloria, Sandy, Joan, Jamie 
 * @Jan 16, 2019
 */
public class CharacterScreen extends World
{

    /**
     * Generates world
     */
    static String character = "waiterCharacter.png";
    public CharacterScreen()
    {    
        // Create a new world with 1200x800 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1); 
        MainCharacterSelection();
    }
    //Adds waiter images as buttons to screen
    private void MainCharacterSelection()
    {
        //initiates buttons & adds to screen
        Button waiter = new Button("waiterCharacter.png", new GameScreen(), true, this, "waiterCharacter.png"); 
        addObject(waiter, 400, 650);
        Button waitress = new Button("waitressCharacter.png", new GameScreen(), true, this, "waitressCharacter.png");
        addObject(waitress, 800, 650);
        //scales images 
        waiter.getImage().scale(110,140);
        waitress.getImage().scale(130,140);
    }
}

