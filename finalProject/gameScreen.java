import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * World for the main game
 * 
 * @Joan, Jamie, Sandy, Gloria
 * @Jan 16, 2019
 */
public class GameScreen extends World
{

    /**
     * Constructor for objects of class gameScreen.
     * 
     */
    //Initializes arrayList to track thought bubbles
    static ArrayList<Label> customerStage = new ArrayList<Label>();
    //Initializes arrayList to track the stage that each customer is on
    ArrayList<String> storedStage = new ArrayList<String>();
    //Initializes arrayList to track each customer
    ArrayList<Customers> customers = new ArrayList<Customers>();
    //spawnNewCharacter checks if a new character should be spawned
    //spawnNewFood checks if a stack of food should be spawned
    //initialSetUp checks if it is the first time the stacks of food are being spawned
    boolean spawnNewCharacter = true, spawnNewFood = true, initialSetUp = true;
    //Checks if the number assigned to each customer should be decreased
    static boolean decreaseAllCustomerNumbers = false;
    //customersWhoLeft tracks number of customers who got angry and left
    //customerIndex tracks the number assigned to the last customer who left
    //pointNumber tracks the number of points that the player has
    //currentCustomer tracks the number that should be assigned to the newest customer
    static int customersWhoLeft = 0, customerIndex = -1, pointNumber = 0, currentCustomer = 0;
    //spawningTime dictates how long before a new customer should spawn in
    //spawnLocation and spawnLocationY decide where the new character should be spawned in at
    //customersInLine tracks the number of customers waiting to be seated
    //currentFood tracks the food item that is being added to the world 
    //spotChecker tracks the number of spawning locations that have been scouted
    int  spawningTime = 16000, spawnLocationX = 350, spawnLocationY = 400, customersInLine = 0, currentFood = 0, spotChecker = 0;
    //initiates timers
    //timer1 decides how long until next customer is spawned in
    //timer2 decides how long until food is spawned in
    SimpleTimer timer, timer2;
    //points is the label that displays the number of points that the player has
    Label time, points, angryCustomers; 
    //cook initializes a new cook
    Cook cook = new Cook();
    //initializes the tables
    static Table blueTable, greenTable, redTable, brownTable;
    //Initializes a scoreboard
    static ScoreBoard sb = new ScoreBoard();
    //this boolean tracks which chairs have people sitting in them
    boolean [] chairTaken = {false, false, false, false, false, false, false, false, false, false, false, false, false} ;
    //Add music to the game 
    GreenfootSound backgroundMusic = new GreenfootSound("game bgm.wav");
    //checks whether or not waiter's image has been decided yet
    boolean loadedImg = false;
    MainCharacter waiter = new MainCharacter(this);
    public GameScreen()
    {    
        // Create a new world with 1200x800 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1);
        //plays music in a loop for the game
        backgroundMusic.playLoop();
        //adds chairs and tables
        addObjects();
        // starts timers
        timer = new SimpleTimer();
        timer.mark();
        timer2 = new SimpleTimer();
        timer2.mark();
        //Initializes points label & adds to screen
        points = new Label("Points: 0", 50);
        angryCustomers = new Label("Customers Who Left: 0", 50);
        addObject(angryCustomers, 675, 50);
        addObject(points, 150, 50);
        //Adds cook object to screen
        addObject(cook, 700, 120);
        //Adds waiter to screen
        addObject(waiter, 400, 200);
        //sets objects in front / behind in order
        setPaintOrder(Food.class, Table.class, Label.class, Customers.class, MainCharacter.class);
    }

    public void act()
    {
        //changes waiter's image based on it's gender
        if(!loadedImg)
        {
            waiter.addedToWorld(CharacterScreen.character);
            loadedImg = true;
        }
        //updates the number of points that the player has & number of customers who left
        points.setValue("Points: " + Integer.toString(pointNumber));
        angryCustomers.setValue("Customers Who Left: " + Integer.toString(customersWhoLeft));
        //checks if there are any customers who need to be removed
        for(int i = 0; i < customers.size(); i++)
        {
            if(customers.get(i).getHappiness() <= 0 && !customers.get(i).isMoving())
            {

                removeObject(customerStage.get(i));
                removeObject(customers.get(i));
                currentCustomer--;
                customersWhoLeft++;
                if(!customers.get(i).getMovable())
                {
                    chairTaken[customers.get(i).getSeatNum()] = false;
                }
                customerStage.remove(i);
                storedStage.remove(i);
                customers.remove(i);
                customerIndex = customers.get(i).getCustomerNumber();
                decreaseAllCustomerNumbers = true; 
            }
            if(customers.get(i).getCurrentStage() > 3)
            {
                if((getObjectsAt(customers.get(i).getX(),customers.get(i).getY() + 40, Food.class).size() == 0))
                {
                    removeObject(customers.get(i));
                    currentCustomer--;
                    chairTaken[customers.get(i).getSeatNum()] = false;
                    pointNumber += customers.get(i).getHappiness()/3;
                    customerIndex = customers.get(i).getCustomerNumber();
                    decreaseAllCustomerNumbers = true;
                    customerStage.remove(i);
                    storedStage.remove(i);
                    customers.remove(i);
                }
            }
        }
        //if a customer has been removed, all customers who are after the removed customer 
        //have their customer numbers decreased
        if(decreaseAllCustomerNumbers)
        {
            for(int i = 0; i < customers.size(); i++)
            {
                if(i >= customerIndex)
                {
                    customers.get(i).decreaseCustomerNumber();
                }
            }
            decreaseAllCustomerNumbers = false;
        }
        //If more than 3 players have left, game ends
        if(customersWhoLeft >= 3)
        {
            String player = Button.getPlayer();
            sb.setScore(player,pointNumber);
            sb.displayScores();
            HighScoreScreen highScoreScreen = new HighScoreScreen();
            Greenfoot.setWorld(highScoreScreen); 
        }
        //If it's time to spawn a new character, and there's less than 12 customers waiting to be seated
        //spawn in a new character
        if(spawnNewCharacter && customersInLine < 12)
        {
            //decides location where character should be spawned in 
            changeLocation();
            //keeps changing spawning location until there's no one in that position already
            //spotChecker makes sure that once all 12 possible positions have been checked, it doesn't keep looping
            while((getObjectsAt(spawnLocationX,spawnLocationY, Customers.class).size() != 0) && spotChecker < 12)
            {
                changeLocation();
                spotChecker++;
            }
            //re-sets number of positions checked to 0
            spotChecker = 0;
            //don't spawn a new character until later
            spawnNewCharacter = false;
            //increases the number of characters in line
            customersInLine++;
            //Creates a new character & adds to screen & arraylist & tells character that it has been added to screen (initializes images)
            Customers temp = new Customers(this);
            temp.addedToWorld();
            customers.add(temp);
            try {
                addObject(customers.get(currentCustomer), spawnLocationX, spawnLocationY);
            }
            catch (Exception e) {
                addObject(customers.get(customers.size()-1), spawnLocationX, spawnLocationY);
            }
            //Creates a new thought bubble & adds to screen & arraylist
            Label tempStage = new Label(temp.getStage());
            customerStage.add(tempStage);
            addObject(customerStage.get(currentCustomer), temp.getXLocation() + 75, temp.getYLocation() - 80);
            //Tracks the stage that character is on & adds to array list
            storedStage.add(temp.getStage());
            //addObject(customers.get(currentCustomer), 600, 400); // IS THIS CODE NECESSARY?
            //assigns a number to the customer
            temp.setCustomerNumber(currentCustomer);
            //Increases the number that should be assigned to customers
            currentCustomer++;

        }

        //Updates thought bubbles & moves it with the character
        for(int i = 0; i < Math.min(customerStage.size(), Math.min(customers.size(), storedStage.size())); i++) 
        {
            //makes thought bubble disappear if customer is eating
            if(customers.get(i).eating()) 
            {
                customerStage.get(i).disappearImage();
            }
            //updates thought bubble
            else if(!(customers.get(i).getStage().equals(storedStage.get(i))))
            {
                customerStage.get(i).changeImage(customers.get(i).getStage());
            }
            //changes location of thought bubble
            customerStage.get(i).setLocation(customers.get(i).getXLocation() + 75, customers.get(i).getYLocation() - 80);

        }

        //checks time that passes before a new character should be spawned in
        if (timer.millisElapsed() > spawningTime) 
        {
            timer.mark();
            spawnNewCharacter = true;
            //decreases customer spawning time & increases speed at which customers get mad
            if(spawningTime > 500)
            {
                spawningTime -= 10;
            }
            if(Customers.angerSpeed > 300)
            {
                Customers.angerSpeed -= 5;
            }

        } 

        //checks if the time is bigger to 5 seconds and if there has already been an initial set up
        if(timer2.millisElapsed() > 5000 && initialSetUp) {
            spawnNewFood = true;
        }

        //spawns in each food in stacks and adds them to the world with specific
        //spacing between them 
        if(spawnNewFood)
        {
            spawnNewFood = false;
            for (int i = 0; i < 3; i++) {
                Food f = new Food();
                f.addedToWorld(currentFood);
                addObject(f, 490 + currentFood*62, 180 + i*5);
            }
            currentFood++;
            //if the current food is equal to 8 then current food resets and
            //starts from zero so there would not be an index out of bounds error
            if(currentFood == 8) {
                currentFood = 0;
                initialSetUp = false;
            }
        }        
        //check if main char passes by table and reorient the order of
        //which class is the most upfront 
        if(MainCharacter.nearTable()) {
            setPaintOrder(Food.class, MainCharacter.class, Table.class, Label.class, Customers.class);
        }
        else {
            setPaintOrder(Food.class, Table.class, Label.class, Customers.class, MainCharacter.class);
        }

    }

    //Adds chairs and tables to screen
    private void addObjects()
    {
        addObject(new Chair(), 420, 435);
        addObject(new Chair(), 500, 435);
        addObject(new Chair(), 580, 435);
        addObject(new Chair(), 820, 435);
        addObject(new Chair(), 900, 435);
        addObject(new Chair(), 980, 435);
        addObject(new Chair(), 420, 635);
        addObject(new Chair(), 500, 635);
        addObject(new Chair(), 580, 635);
        addObject(new Chair(), 820, 635);
        addObject(new Chair(), 900, 635);
        addObject(new Chair(), 980, 635);

        blueTable = new Table();
        blueTable.setImage("blue table.png");
        addObject(blueTable, 500, 450);
        greenTable = new Table();
        greenTable.setImage("green table.png");
        addObject(greenTable, 500, 650);
        redTable = new Table();
        redTable.setImage("red table.png");
        addObject(redTable, 900, 450);
        brownTable = new Table();
        brownTable.setImage("brown table.png");
        addObject(brownTable, 900, 650);
        TrashCan trashCan = new TrashCan();
        addObject(trashCan, 1150, 750);
    }

    //restocks the food in stacks if the food is all gone 
    public void restock(int currFood) {
        for (int i = 0; i < 3; i++) {
            Food f = new Food();
            f.addedToWorld(currFood);
            addObject(f, 490 + currFood*62, 180 + i*5);
        }
    }

    //decides location that characters will be spawned in, spawns right to left
    private void changeLocation()
    {
        if(spawnLocationX > 50)
        {
            spawnLocationX -= 100;

        }
        else
        {
            spawnLocationX = 250;
            spawnLocationY += 100;
            if(spawnLocationY == 800 && spawnLocationX == 250)
            {
                spawnLocationX = 250;
                spawnLocationY = 400;
            }
        }
    }

}
