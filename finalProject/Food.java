import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 *This is the Food class. it allows the foods/drinks to be picked up, dropped off and served to the customers. 
 * 
 * @Jamie, Joan, Gloria, Sandy 
 * @version (a version number or a date)
 */
public class Food extends Actor
{
    //initiates an array for the food images
    private GreenfootImage[] images = new GreenfootImage[8];
    //checks to see if food is served to the right customer 
    private boolean timeToEat = false;
    private int timer = 0;
    //
    private int food = -1;
    //
    private boolean initialPick = true;
    private boolean foodEaten = false;
    public void act() 
    {
        //if timeToEat is true then timer begins to increment 
        if(timeToEat) {
            timer++;
        }
        //food disappears after 7 seconds to signify they have "ate"
        if(timer > 420) 
        {
            //food that has been ate is then removed 
            getWorld().removeObject(this);
            GameScreen.pointNumber += 20;
        }
        //trash can. When food is dropped off at the trash can it is deleted from the world.
        try {
            if (Greenfoot.isKeyDown("e") && this.getY() >= 710 && this.getX() >= 1110){
                getWorld().removeObject(this);
            }
        }
        catch (Exception e) {
        }
    }  
    //
    public void addedToWorld(int currentFood)
    {
        images[currentFood] = new GreenfootImage("food"+ currentFood + ".png");
        setImage(images[currentFood]);
        food = currentFood;
    }

    public int getFoodType() {
        return food;
    }
    //getter method for initialPick
    public boolean getInitialPick() {
        return initialPick;
    }
    //setter method for initialPick
    public void setInitialPick() {
        initialPick = false;
    }

    public void checkTimeToEat() {
        //triggered when food is placed on table
        if(customerNearFood()) {
            setTimer();
        }
    }
    //setter method for 
    private void setTimer() {
        timeToEat = true;
    }

    private boolean customerNearFood() {
        //check if customer is near food, and if they want it or not
        try {
            Customers nearestCustomer = new Customers(null);
            List<Customers> nearCustomers = getObjectsInRange(70, Customers.class);
            double nearestDistance = 100.0;
            double distance = 100.0;
            for (int i = 0; i < nearCustomers.size(); i++) {
                distance = getDistance(nearCustomers.get(i));
                if (distance < nearestDistance) {
                    nearestCustomer = nearCustomers.get(i);
                    nearestDistance = distance;
                }
            }
            if(nearestCustomer.getFoodPreference() == getFoodType()) {
                nearestCustomer.startEating();
                foodEaten = true;
                return true;
            }
        }
        catch(Exception e) {
        }
        foodEaten = false;
        return false;
    }
    //helper function
    private double getDistance(Customers customer) {
        return Math.hypot(customer.getX() - getX(), customer.getY() - getY());
    }

    public boolean getFoodEaten() {
        return foodEaten;
    }
}
