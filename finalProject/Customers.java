import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * Customer Object
 * 
 * @Joan, Gloria, Jamie, Sandy 
 * @Jan 16, 2019
 */
public class Customers extends Actor
{
    //customerNumber tracks the number associated with the customer
    //secondChoice tracks the table that the customer wants to be seated at
    //lineX and lineY tracks customer's location before they are dragged
    private int customerNumber,  secondChoice = -1, lineX, lineY;
    //happiness tracks the character's happiness
    //character tracks which type of customer this character is
    //stage tracks the food that the character is eating
    //num tracks the image that is being used for the customer
    //foodChoice tracks the food that the customer wants
    public int happiness = 100, character = -1, stage = 0, changeFrame = 3, num = 1, foodChoice = -1;
    //speed at which customers get angry
    static int angerSpeed = 1000;
    //isSeated checks whether or not the customer is seated
    //isGrabbed checks whether or not the customer is being dragged by the mouse
    //movable checks whether or not the customer can be dragged
    //isEating checks if the customer is eating
    //firstTime checks if customer's happiness has already been increased from getting food
    private boolean  isSeated = false, isGrabbed, movable = true,isEating = false, customerEat = false, firstTime = true;
    //initiates an array for images
    private GreenfootImage[] images = new GreenfootImage[3];
    //initiates timers
    SimpleTimer timer, timer2;
    GameScreen gs = new GameScreen();
    //checks if tables are full
    boolean tableOneIsFull = false;
    boolean tableTwoIsFull = false;
    boolean tableThreeIsFull = false;
    boolean tableFourIsFull = false;
    int seatNum = 0;
    //Initiates customer
    public Customers(GameScreen gs)
    {
        this.gs = gs;
        //Randomly decides character type
        character = Greenfoot.getRandomNumber(6);
        //starts timers
        timer = new SimpleTimer();
        timer.mark();
        timer2 = new SimpleTimer();
        timer2.mark();
    }

    //loads in images 
    public void addedToWorld()
    {
        for(int i = 0; i < images.length; i++)
        {
            images[i] = new GreenfootImage("customer"+ character + "Eating" + i + ".png");
        }
        setImage(images[0]);
    }

    public void act() 
    {
        //checks if customer is eating
        if(customerEat && !isEating)
        {
            //changes the stage that the customer is on
            changeStage();
            customerEat = false;
            isEating = true;
            //adds 20 points
            GameScreen.pointNumber += 20;
        }
        //checks if there is food in front of the customer
        if((getWorld().getObjectsAt(this.getX(),this.getY() + 40, Food.class).size() == 0) )
        {
            isEating = false;

        }  
        /*
         * If customer is eating
         * Changes images used so that customer looks like it's eating   
         */
        if(isEating)
        {
            if (timer.millisElapsed() > (100)) 
            {
                if(num >= images.length)
                {
                    num = 1;
                }
                setImage(images[num]);
                num++;
                timer.mark();
            }
            //If it's the first time the code has run, increase the character's happiness
            if(firstTime)
            {
                this.changeHappiness(10);
                firstTime = false;
            }
        }
        else
        {
            firstTime = true;
            //reverts image to default standing image
            setImage(images[0]);
            //decreases customer's happiness
            if(timer2.millisElapsed() > angerSpeed)
            {
                this.changeHappiness(-1);
                timer2.mark();
            }
        }
        //changes default image to livid if happiness is under 33
        if(happiness < 33)
        {
            images[0] = new GreenfootImage("customer"+ character + "Angry" + 3 + ".png");
        }
        //changes default image to mad if happiness is under 66
        else if(happiness < 66)
        {
            images[0] = new GreenfootImage("customer"+ character + "Angry" + 2 + ".png");
        }
        //changes default image to normal if happiness is not under 66
        else
        {
            images[0] = new GreenfootImage("customer"+ character + "Angry" + 1 + ".png");
        }

        //if the customer is draggable (hasn't already been seated
        if(movable)
        {
            // check for initial pressing down of mouse button
            if (Greenfoot.mousePressed(this) && !isGrabbed)
            {
                // grab the object
                isGrabbed = true;
                lineX = this.getX();
                lineY = this.getY();
                // the rest of this block will avoid this object being dragged UNDER other objects
                World world = getWorld();
                MouseInfo mi = Greenfoot.getMouseInfo();
                world.removeObject(this);
                world.addObject(this, mi.getX(), mi.getY());
                return;
            }
            // check for actual dragging of the object
            if ((Greenfoot.mouseDragged(this)) && isGrabbed)
            {
                // follow the mouse
                MouseInfo mi = Greenfoot.getMouseInfo();
                setLocation(mi.getX(), mi.getY());
                return;
            }
            // check for mouse button release
            if (Greenfoot.mouseDragEnded(this) && isGrabbed)
            {
                // release the object
                isGrabbed = false;
                //If customer is within the dining area
                if(this.getX() > 370 && this.getX() < 1020 && this.getY() < 735 && this.getY() > 335)
                {
                    if (gs.chairTaken[1]== true &&gs.chairTaken[2]== true && gs.chairTaken[3]== true){
                        tableOneIsFull = true;
                    }
                    if (gs.chairTaken[4]== true && gs.chairTaken[5]== true && gs.chairTaken[6]== true){
                        tableTwoIsFull = true;
                    }
                    if (gs.chairTaken[7]== true &&gs.chairTaken[8]== true && gs.chairTaken[9]== true){
                        tableThreeIsFull = true;
                    }
                    if (gs.chairTaken[10]== true &&gs.chairTaken[11]== true && gs.chairTaken[12]== true){
                        tableFourIsFull = true;
                    }
                    if(this.getY() > 335 && this.getY() < 535)
                    {
                        if(this.getX() < 620 && tableOneIsFull !=true)
                        {
                            if(this.getX() < 460 && gs.chairTaken[1] == false)
                            {
                                putCustomerInPlace(420, 395, 1);
                            }
                            else if (this.getX() < 540 && gs.chairTaken[2] == false)
                            {
                                putCustomerInPlace(500, 395, 2);
                            }
                            else if (gs.chairTaken[3] == false)
                            {
                                putCustomerInPlace(580, 395, 3);
                            }
                            else if(!gs.chairTaken[1])
                            {
                                putCustomerInPlace(420, 395, 1);
                            }
                            else{
                                this.setLocation(lineX, lineY);
                                movable = true;
                            }
                        }
                        else if(!tableTwoIsFull )
                        {

                            if(this.getX() < 860 && gs.chairTaken[4] == false)
                            {
                                putCustomerInPlace(820, 395, 4);
                            }
                            else if (this.getX() < 940 && gs.chairTaken[5] == false)
                            {
                                putCustomerInPlace(900, 395, 5);
                            }
                            else if (gs.chairTaken[6] == false)
                            {
                                putCustomerInPlace(980, 395, 6);
                            }
                            else if(!gs.chairTaken[4])
                            {
                                putCustomerInPlace(820, 395, 4);
                            }
                            else{
                                this.setLocation(lineX, lineY);
                                movable = true;
                            }
                        }

                        else{
                            this.setLocation(lineX, lineY);
                            movable = true;
                        }
                    }
                    else if(this.getY() > 535 && this.getY() < 735)
                    {
                        if(this.getX() < 620 && !tableThreeIsFull)
                        {

                            if(this.getX() < 460 && gs.chairTaken[7] == false)
                            {
                                putCustomerInPlace(420, 595, 7);
                            }
                            else if (this.getX() < 540 && gs.chairTaken[8] == false)
                            {
                                putCustomerInPlace(500, 595, 8);
                            }
                            else if (gs.chairTaken[9] == false)
                            {
                                putCustomerInPlace(580, 595, 9);
                            }
                            else if(!gs.chairTaken[7])
                            {
                                putCustomerInPlace(420, 595, 7);
                            }
                            else{
                                this.setLocation(lineX, lineY);
                                movable = true;
                            }
                        }
                        else if(!tableFourIsFull)
                        {

                            if(this.getX() < 860 && gs.chairTaken[10] == false)
                            {
                                putCustomerInPlace(820, 595, 10);
                            }
                            else if (this.getX() < 940 && gs.chairTaken[11] == false)
                            {
                                putCustomerInPlace(900, 595, 11);
                            }
                            else if (gs.chairTaken[12] == false)
                            {
                                putCustomerInPlace(980, 595, 12);
                            }
                            else if(!gs.chairTaken[10])
                            {
                                putCustomerInPlace(820, 595, 10);
                            }
                            else{
                                this.setLocation(lineX, lineY);
                                movable = true;
                            }
                        }
                        else{
                            this.setLocation(lineX, lineY);
                            movable = true;
                        }
                    }
                    //checks if character is sitting at the right table
                    switch(secondChoice)
                    {
                        case 0:
                        if(this.getY() == 395 && this.getX() < 600)
                        {
                            GameScreen.pointNumber += 20;
                            happiness += 10;
                        }
                        break;

                        case 1:
                        if(this.getY() == 595 && this.getX() > 600)
                        {
                            GameScreen.pointNumber += 20;
                            happiness += 10;
                        }
                        break;

                        case 2:
                        if(this.getY() == 395 && this.getX() > 600)
                        {
                            GameScreen.pointNumber += 20;
                            happiness += 10;
                        }
                        break;

                        case 3:
                        if(this.getY() == 595 && this.getX() < 600)
                        {
                            GameScreen.pointNumber += 20;
                            happiness += 10;
                        }
                        break;
                    }
                }
                //returns customer to original location
                else
                {
                    this.setLocation(lineX, lineY);
                }
                return;
            }
        }

    }    

    //Randomly choose table customer wants to sit at
    public int seatingReq()
    {

        secondChoice = Greenfoot.getRandomNumber(4);

        return secondChoice;
    }

    //randomly chooses food customer wants
    public int chooseFood()
    {
        foodChoice = Greenfoot.getRandomNumber(7) + 1;
        return foodChoice;
    }

    //gets the thing that the customer currently wants
    public String getStage()
    {
        if(stage != 0)
        {
            if(foodChoice == -1)
            {
                return "thoughtBubbleFood" + chooseFood() + ".png";
            }
            else
            {
                return "thoughtBubbleFood" + foodChoice + ".png";
            }
        }
        else if(stage == 0)
        {
            if(secondChoice == -1)
            {
                return "thoughtBubbleTable" + seatingReq() + ".png";
            }
            else
            {
                return "thoughtBubbleTable" + secondChoice + ".png";
            }
        }

        return "thoughtBubbleFood1.png";
    }

    //gets the number of the stage the customer is on
    public int getCurrentStage()
    {
        return stage;
    }

    //changes customer happiness
    public void changeHappiness(int changeInHappiness)
    {
        happiness += changeInHappiness;
    }

    //gets customer's happiness
    public int getHappiness()
    {
        return happiness;
    }

    //gets the customer's number
    public int getCustomerNumber()
    {
        return customerNumber;
    }

    //changes the stage that the customer is on & randomizes food that customre wants
    public void changeStage()
    {
        stage++;
        foodChoice = Greenfoot.getRandomNumber(7) + 1;
    }

    //gets customer's location
    public int getXLocation()
    {
        return this.getX();
    }

    public int getYLocation()
    {
        return this.getY();
    }

    public int getFoodPreference() {
        return foodChoice-1;
    }

    //returns whether or not customer is eating
    public boolean eating()
    {
        return isEating;
    }

    //sets whethether or not customer is eating
    public void setEatingState(boolean choice)
    {
        isEating = choice;
    }

    public void startEating() {
        customerEat = true;
    }

    //sets customer number
    public void setCustomerNumber(int n)
    {
        this.customerNumber = n;
    }

    //decreases customer number
    public void decreaseCustomerNumber()
    {
        customerNumber--;
    }

    public boolean getMovable()
    {
        return movable;
    }

    public int getSeatNum()
    {
        return seatNum;
    }

    public boolean isMoving()
    {
        return isGrabbed;
    }

    public void customerHasASeat()
    {
        //It will be seated, and so it can't be moved anymore
        movable = false;
        //changes character stage
        changeStage();
        //decreases number of characters in line
        gs.customersInLine--;
        //snaps customer into place
    }
    
    //x and y are the location that customer is put in
    //seatNumber is seat customer is sitting in
    public void putCustomerInPlace(int x, int y, int seatNumber)
    {
        this.setLocation(x, y);
        gs.chairTaken[seatNumber] = true;
        this.customerHasASeat();
        seatNum = seatNumber;
    }
}
