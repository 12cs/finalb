import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Initiates world for instructions
 * 
 * @Gloria, Sandy, Jamie, Joan
 * @Jan 16, 2019
 */
public class InstructionScreen extends World
{

    /**
     * Creates basic world
     */
    public InstructionScreen()
    {    
        // Create a new world with 1200x800 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1); 
        Buttons();
    }

    //Adds button to screen
    private void Buttons()
    {
        Button button = new Button("next button.png",new CharacterScreen(), false, this, "");
        addObject(button, 1070, 600);
        button.getImage().scale(220,80);
    }
}
