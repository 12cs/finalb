import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Graphics2D;
import java.awt.FontMetrics;
import java.awt.font.TextLayout;

/**
 * This class is the label used to show the score and thought bubbles
 * 
 * @Gloria, Jamie, Joan, Sandy 
 * @Jan 16, 2019
 */
public class Label extends Actor
{
    //declares variable to hold value that will be displayed
    private String value;
    //declares default font size
    private int fontSize;
    //initializes default colours
    private Color lineColor = Color.BLACK;
    private Color fillColor = Color.WHITE; 
    private static final Color transparent = new Color(0,0,0,0);
    //public static int money = 0;
    //declares variable to hold points
    private int points = 0;
    /*
     * Creates a new label.
     * Initializes it with the needed text and the font size.
     * (Used to hold the score) 
     */
    public Label(String value, int fontSize) 
    {
        this.value = value;
        this.fontSize = fontSize;
        updateImage();
    }    
    //creates new label to hold an image
    public Label(String img)
    {
        setImage(img);
    }
    //sets value of score
    public void setValue(int value)
    {
        this.value = Integer.toString(value);
        updateImage();
    }
    //sets value of word Counter
    public void setValue(String value)
    {
        this.value = value;
        updateImage();
    }
    //resizes label 
    public void changeImage(String img)
    {
        GreenfootImage temp = new GreenfootImage(img);
        setImage(temp);
    }
    // temporarily hides the image
    public void disappearImage()
    {
        GreenfootImage temp = new GreenfootImage(100, 100);
        temp.setTransparency(0);
        setImage(temp);
    }
    //updates label
    private void updateImage()
    {
        setImage(new GreenfootImage(value, fontSize, fillColor, transparent, lineColor));
    }
}
