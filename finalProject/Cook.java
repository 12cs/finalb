import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Animates Cook
 * 
 * @Jamie, Gloria, Sandy, Joan
 * @Jan 16, 2019
 */
public class Cook extends Actor
{
    // initiates array to hold images
    private GreenfootImage[] images = new GreenfootImage[17];
    // initiates a timer
    SimpleTimer timer;
    //num stores the image that is currently being displayed
    int num = 1;

    //Cook constructor - instantiates all images
    public Cook()
    {
        //loads images into array
        for(int i = 1; i < images.length; i++)
        {
            images[i] = new GreenfootImage("chefToss" + i + ".png");
        }
        //sets image to default standing one
        setImage(images[0]);
        //Initiates and starts the timer
        timer = new SimpleTimer();
        timer.mark();
    }

    //Continuously changes chef animations
    public void act() 
    {
        if (timer.millisElapsed() > (50)) 
        {
            if(num >= images.length)
            {
                num = 1;
            }
            setImage(images[num]);
            num++;
            timer.mark();
        }

    }    
}