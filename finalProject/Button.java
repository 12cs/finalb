import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*;
import java.net.URL;
import javax.imageio.ImageIO;
import java.awt.GridLayout;

/**
 * Button that user can press
 * 
 * @Gloria, Jamie, Joan, Sandy
 * @Jan 16, 2019
 */
public class Button extends Actor
{
    //specifies which world to direct screen to
    World world;
    //previous world
    World pWorld;
    //checks to see if program needs to ask for user name
    Boolean promptName = false;
    //player name
    static String player;
    //Name of button 
    public String name;
    //playerRes is an int that tells the computer what has been pressed in the prompt(Enter == 0, cancel == 2, exit button == -1)
    int playerRes; 
    public void act() 
    {
        //checks if button has been pressed
        if(Greenfoot.mouseClicked(this))
        {
            //sets the gender of the waiter
            CharacterScreen.character = this.name;
            //checks if program needs to ask for user name
            if(this.promptName) 
            {
                playerName();
                //if the player hits cancel or exit button, they go back to the previous screen 
                if(playerRes == 2 || playerRes == -1) 
                {
                    Greenfoot.setWorld(pWorld);
                }
                else 
                {
                    Greenfoot.setWorld(world);
                }
            }
            else 
            {
                Greenfoot.setWorld(world);
            }
        }
    }   

    /*initializes button, 
     * String img : gives image location, World worlTemp gives world to direct towards
     * Boolean promptName specifies whether or not player needs to be asked for name
     * String name gives name of button
     */
    public Button(String img, World worldTemp, Boolean promptName, World prevWorld, String name)
    {
        world = worldTemp;
        //sets button image
        GreenfootImage start = new GreenfootImage(img);
        setImage(start);
        //alters value that decides whether or not you need to ask user for name
        this.promptName = promptName;
        pWorld = prevWorld;
        //player = null;
        this.name = name;

    }
    //opens dialog box for player to input name
    private void playerName() 
    {
        JFrame frame = new JFrame("Player's name information.");

        frame.setAlwaysOnTop(true);
        JTextField pName = new JTextField();
        JOptionPane jop = new JOptionPane();

        jop.showMessageDialog(frame, "Are you ready to start the game?"); 
        //playerRes is an int that tells the computer what has been pressed in the prompt(Enter == 0, cancel == 2, exit button == -1)
        playerRes = jop.showConfirmDialog(frame, pName, "Player's name:", JOptionPane.OK_CANCEL_OPTION);  
        player = pName.getText();
        if(player != null && playerRes == 0) //If a blank entry is given
        { 
            //prompt player to try again
            if("".equals(player.trim())) 
            {
                JOptionPane.showMessageDialog(null, "Please enter your name first!");
                playerName();
            }
        }
    }
    //returns player name
    public static String getPlayer() 
    {
        //System.out.println(player);
        return player;
    }
    //sets player name
    public static void setPlayer(String newPlayer) 
    {
        player = newPlayer;
    }

}
