import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Initiates world for scoreborad
 * 
 * @Joan, Jamie, Sandy, Gloria 
 * @Jan 16, 2019
 */
public class HighScoreScreen extends World
{

    /**
     * Creates basic world
     */
    public HighScoreScreen()
    {    
        // Create a new world with 1200x800 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1);
        //adds scoreboard to screen
        addObject(GameScreen.sb, 438, 505);
        //adds buttons to screen
        Buttons();
        //resets all values
        GameScreen.customersWhoLeft = 0;
        GameScreen.pointNumber = 0; 
        GameScreen.currentCustomer = 0;
        GameScreen.customerStage = new ArrayList<Label>();
        GameScreen.decreaseAllCustomerNumbers = false;
        Customers.angerSpeed = 1000;
    }
    //initializes start & instruction buttons and adds to screen
    private void Buttons()
    {
        Button start = new Button("start button.png",new CharacterScreen(), false, this, "");
        addObject(start, 1050, 550);
        Button instructions = new Button("instructions button.png", new InstructionScreen(), false, this, "");
        addObject(instructions, 1050, 700);
    }
}
