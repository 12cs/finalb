import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*; 
/**
 * Object for the scoreboard
 * 
 * @Joan, Jamie, Sandy, Gloria 
 * @Jan 16, 2019
 */
public class ScoreBoard extends Actor
{
    //name = the name that the user inputted, scoreValue = user's score, output is the thing to print
    // to screen
    private String name, scoreValue, output;
    //HashMap stores highest score
    static HashMap<String,Integer> scores;
    int wordSize = 40;
    /**
     * HashMap is initialized in the constructor 
     */
    public ScoreBoard() 
    {
        scores = new HashMap<String,Integer>(); 
    }    

    /*
     * If the user's name is already in the hashmap and they beat their personal highscore,
     * replace their old highscore with their new highscore. Otherwise, if the user's 
     * name is not already in the hashmap, put it in. 
     */
    public void setScore(String name, int scoreValue)
    {    
        Integer value = scores.get(name); 
        if(value != null)
        {
            if(scoreValue > value)
            {
                scores.replace(name,scoreValue); 
            }
        }
        else 
        {
            scores.put(name, scoreValue);
        }
    }

    /*
     * Displays the scores in the Hashmap with the highest score at the top of the list.
     */
    public void displayScores() 
    {
        //Initializes output as empty string
        output = "" ;
        //Initiates a new array for scores
        int[] sorting = new int[scores.size()];
        //Sets iterator to 0
        int i = 0;
        //Adds all scores into array
        for(String temp : scores.keySet())
        {
            sorting[i] = scores.get(temp);
            i++;
        }

        //sorts values in array using merge sort
        mergeSort(sorting);

        //creates a clone of hashmap
        HashMap<String,Integer> scores2 = (HashMap) scores.clone();
        //creates new array to store the stuff that will be printed out
        String[] printing = new String[scores.size()];
        // loops through the hashmap to add the strings into the array
        for(int j = sorting.length - 1; j > -1; j--)
        {
            //For the selected string in the keyset
            for(String temp : scores2.keySet())
            {
                //If the associated value is equal to the current value of the array
                //add the string that's going to be printed out to the printing array
                // and remove that key from the hashmap
                if (sorting[j] == scores2.get(temp))
                {

                    printing[j] =  temp + ": " + sorting[j] + "\n";
                    scores2.remove(temp);
                    break;
                }
            }
        }

        //Add all the strings to the output string
        for(int j = sorting.length - 1; j > -1; j--)
        {
            output += printing[j];
        }
        //changes size of scores
        int calculationHelper= scores.size() / 5;
        wordSize = 40 - (calculationHelper * 5);
        setImage(new GreenfootImage(output, wordSize, Color.BLACK, new Color(0,0,0,0), new Color(0,0,0,0)));
    }

    //mergesort method
    public void mergeSort(int[] arr) 
    {
        int[] temp = new int[arr.length];
        mergeSortHelper(arr, 0, arr.length - 1, temp);
    }

    //helper method for mergesort
    private void mergeSortHelper(int[] arr, int from, int to, int[] temp)
    {
        // If the array length is greater than 1
        if(to - from >= 1)
        {
            int mid = (from + to) / 2;
            mergeSortHelper(arr, from, mid, temp);
            mergeSortHelper(arr, mid + 1, to, temp);
            merge(arr, from, mid, to, temp);
        }
    }

    //merges the array together in order
    private void merge(int[] arr, int from, int mid, int to, int[] temp) 
    {
        int i = from;       // track left array position
        int j = mid + 1;    // track right array position
        int k = from;       // track temp position

        while(i <= mid && j <= to)
        {
            // If the element in the left subarray is less
            // than the element in the right subarray it 
            // is next in the merged list
            if(arr[i] < arr[j])
            {
                temp[k] = arr[i];
                i++;
            }
            else
            {
                temp[k] = arr[j];
                j++;
            }
            k++;
        }

        // We may have missed elements from either list
        while(i <= mid)
        {
            temp[k] = arr[i];
            i++;
            k++;
        }

        while(j <= to)
        {
            temp[k] = arr[j];
            j++;
            k++;
        }

        // Copy over from temp to elements
        for(k = from; k <= to; k++)
        {
            arr[k] = temp[k];
        }

    }
}
